import { Component, OnInit, ViewChild } from '@angular/core';
import { } from '@types/googlemaps';

import { DisruptionService } from '../disruption.service';
import { Disruption } from '../disruption';
import { DISRUPTIONS } from '../mock-disruption';

import { DisruptionHttpService } from '../disruption-http.service';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css'],
  providers: [DisruptionService, DisruptionHttpService]
})
export class GoogleMapComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  disruptions: Disruption[] = [];

  constructor(private disruptionService: DisruptionService, private disruptionHttpService: DisruptionHttpService) { }

  ngOnInit() {
    this.getHTTPDisruptions();
    this.loadMap();
  }

  /***************************************************************************/
  /***************************************************************************/
  getDisruptions(): void {
    this.disruptionService.getDisruptions().subscribe(disruptions => this.disruptions = disruptions);
  }

  /***************************************************************************/
  /***************************************************************************/
  getHTTPDisruptions(): void {
    this.disruptionHttpService.getDisruption().subscribe(disruptions => {
      disruptions["Response"].forEach(response => {
        this.disruptions.push(response);
        this.loadMarkers();
      });
    });
    console.log(this.disruptions);
  }



  /***************************************************************************/
  /***************************************************************************/
  loadMap(): void {
    const myLatLng = { lat: 51.5073509, lng: -0.1277583 };

    const mapProp = {
      center: new google.maps.LatLng(myLatLng.lat, myLatLng.lng),
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.TERRAIN
    };

    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
  }

  /***************************************************************************/
  /***************************************************************************/
  loadMarkers(): void {
    const icon = {
      url: "../../assets/ic_build_black_24dp_2x.png",
      scaledSize: new google.maps.Size(25, 25),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 0)
    };


    var infowindow = new google.maps.InfoWindow();

    this.disruptions.forEach(disruption => {
      const marker = new google.maps.Marker({
        position: disruption,
        map: this.map,
        title: disruption.locationName,
        icon: icon
      });

      const contentString =
        '<h4 style="text-align: center;">' + disruption.locationName + '</h4>' +
        '<div>' +
        '<hr/>' +
        '<p style="text-align: center;">' + disruption.description + '</p>' +
        '<hr/>' +
        '<p style="text-align: center;"> <b>' + disruption.fromDate.slice(0, 10) + '</b>  to  <b>' + disruption.toDate.slice(0, 10) + '</b></p>' +
        '</div>';

      google.maps.event.addListener(marker, 'click', function () {
        infowindow.close();
        this.map.setZoom(13);
        infowindow.setContent(contentString);
        infowindow.setOptions({ maxWidth: 200 });
        infowindow.open(this.map, marker);
      });

    });
  }

}
