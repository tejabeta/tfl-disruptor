import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Disruption } from './disruption';
import { DISRUPTIONS } from './mock-disruption';

@Injectable()
export class DisruptionService {

  getDisruptions(): Observable<Disruption[]> {
    return of(DISRUPTIONS);
  }

  constructor() { }

}
