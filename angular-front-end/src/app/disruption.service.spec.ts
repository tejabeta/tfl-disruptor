import { TestBed, inject } from '@angular/core/testing';

import { DisruptionService } from './disruption.service';

describe('DisruptionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DisruptionService]
    });
  });

  it('should be created', inject([DisruptionService], (service: DisruptionService) => {
    expect(service).toBeTruthy();
  }));
});
