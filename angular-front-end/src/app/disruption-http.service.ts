import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DisruptionHttpService {

  constructor(private http: HttpClient) { }

  getDisruption() {
    const disruptionURL = "http://localhost:8001/disruption"
    return this.http.get(disruptionURL);
  }

}
