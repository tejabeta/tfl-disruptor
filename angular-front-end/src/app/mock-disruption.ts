import { Disruption } from './disruption';

export const DISRUPTIONS: Disruption[] = [
    {
        fromDate: "2015-11-15T03:30:00Z",
        toDate: "2018-07-31T01:29:00Z",
        description: "Until 2018. For York Road and the Southbank follow signs via the National Rail concourse.",
        locationName: "Waterloo Underground Station",
        mode: "tube",
        appearance: "PlannedWork",
        lat: 51.503097,
        lng: -0.11512
    },
    {
        fromDate: "2017-01-10T04:30:00Z",
        toDate: "2018-12-31T01:29:00Z",
        description: "Queues are likely at busy times when changing between the Victoria line and District/Circle line platforms due to reconstruction works.",
        locationName: "Victoria Underground Station",
        mode: "tube",
        appearance: "PlannedWork",
        lat: 51.49645,
        lng: -0.144899
    }
]