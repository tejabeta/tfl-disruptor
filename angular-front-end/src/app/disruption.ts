export class Disruption {
    mode: string;
    locationName: string;
    lat: any;
    lng: any;
    description: string;
    fromDate: any;
    toDate: any;
    appearance: string;
}