import { TestBed, inject } from '@angular/core/testing';

import { DisruptionHttpService } from './disruption-http.service';

describe('DisruptionHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DisruptionHttpService]
    });
  });

  it('should be created', inject([DisruptionHttpService], (service: DisruptionHttpService) => {
    expect(service).toBeTruthy();
  }));
});
