# TFL Disruptor

##About

This is a web application that showcases the live disruptions for TFL tube service. Built with the help of Angular 5 and Tornedo.    

## Development server front-end

Chage directory to angular-front-end, and Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Development server back-end

Chage directory to tornado-back-end, and Run `python server.py` for a dev server. Navigate to `http://localhost:8001/disruption`.