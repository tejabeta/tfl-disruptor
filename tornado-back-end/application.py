from url import urls
import tornado.web

app = tornado.web.Application(
    handlers=urls,
    debug=True
)
