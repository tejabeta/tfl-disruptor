import tornado.gen
import tornado.web
import tornado.httpclient
import json
from model.disruption import Disruption


class DisruptionRepository(object):

    @tornado.gen.coroutine
    def get_disruption(self):
        total_disruptions = []
        URL = "https://api.tfl.gov.uk/StopPoint/Mode/tube/Disruption"
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = yield http_client.fetch(URL)
        count = 0
        for each_disruption in json.loads(response.body):
            if count < 25:
                latlng = yield self.get_gps(each_disruption["commonName"].replace(" ", "%20"))
                disruption = Disruption(
                    each_disruption["fromDate"],
                    each_disruption["toDate"],
                    each_disruption["description"],
                    each_disruption["commonName"],
                    each_disruption["mode"],
                    each_disruption["appearance"],
                    latlng["lat"],
                    latlng["lng"])

                dict_disruption = {"fromDate": disruption.get_from_date(),
                                   "toDate": disruption.get_to_date(),
                                   "locationName": disruption.get_location_name(),
                                   "mode": disruption.get_mode(),
                                   "description": disruption.get_description(),
                                   "appearance": disruption.get_appearence(),
                                   "lat": disruption.get_lat(),
                                   "lng": disruption.get_lng()
                                   }

                total_disruptions.append(dict_disruption)
                count += 1
        raise tornado.gen.Return(total_disruptions)

    @tornado.gen.coroutine
    def get_gps(self, stationName):
        URL = "https://api.tfl.gov.uk/StopPoint/Search?query=" + stationName
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = yield http_client.fetch(URL)

        latlngResponse = {
            "lat": json.loads(response.body)["matches"][0]["lat"],
            "lng": json.loads(response.body)["matches"][0]["lon"]
        }

        raise tornado.gen.Return(latlngResponse)
