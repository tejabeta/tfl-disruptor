import tornado.escape
import tornado.ioloop
import tornado.web
from tornado.options import define, options, parse_command_line

from url import urls
from application import app

define("port", type=int, default=8001, help="Runs on the given port")
define("process", default=1, help="<=0 to run based on cpu cores", type=int)


def main():
    tornado.options.parse_command_line()
    server = tornado.httpserver.HTTPServer(app)
    server.bind(options.port)
    server.start(options.process)
    print("Server started on port: ", options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
