import tornado.web
import tornado.gen
import tornado.httpclient
from repository.disruption_repository import *


class DisruptionHandler(tornado.web.RequestHandler):
    @tornado.gen.coroutine
    def get(self):
        try:
            response = yield DisruptionRepository().get_disruption()
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header('Access-Control-Allow-Methods', 'GET')
            response1 = {'Response': response}
            self.write(response1)
        except Exception as e:
            print("Error occured in disruption handler get method")
            self.send_error(500)
