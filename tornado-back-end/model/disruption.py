class Disruption(object):

    def __init__(self, from_date=None, to_date=None, description=None, location_name=None, mode=None, appearence=None, lat=None, lng=None):
        self.from_date = from_date
        self.to_date = to_date
        self.description = description
        self.location_name = location_name
        self.mode = mode
        self.appearence = appearence
        self.lat = lat
        self.lng = lng

    def set_from_date(self, from_date):
        self.from_date = from_date

    def get_from_date(self):
        return self.from_date

    def set_to_date(self, to_date):
        self.to_date = to_date

    def get_to_date(self):
        return self.to_date

    def set_description(self, description):
        self.description = description

    def get_description(self):
        return self.description

    def set_location_name(self, location_name):
        self.location_name = location_name

    def get_location_name(self):
        return self.location_name

    def set_mode(self, mode):
        self.mode = mode

    def get_mode(self):
        return self.mode

    def set_appearence(self, appearence):
        self.appearence = appearence

    def get_appearence(self):
        return self.appearence

    def set_lat(self, lat):
        self.lat = lat

    def get_lat(self):
        return self.lat

    def set_lng(self, lng):
        self.lng = lng

    def get_lng(self):
        return self.lng
